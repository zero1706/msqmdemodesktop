﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Messaging;

namespace DEMOMSQMDESKTOP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BarraSuperior_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void RecibirMsg_Click(object sender, EventArgs e)
        {
            using (MessageQueue Testing = new MessageQueue())
            {

                Testing.Path = @".\private$\Testing";
                System.Messaging.Message msg = new System.Messaging.Message();
                msg = Testing.Receive(new TimeSpan(0,0,5));
                msg.Formatter = new XmlMessageFormatter(new string[] { "System.String,mscorlib"});
                string mensaje = msg.Body.ToString();

                TextRecibidoCentro.Text = mensaje;

                MessageBox.Show( "Message Recibido!");

            }
        }
        public struct Payment
        {
            public int Id;
            public string Usuario;

            public string Beneficiario;
            public DateTime Fecha;

            public string Producto;
            public string Precio;

            public string Email;
            public string Direccion;
        }

        private void BotonCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
